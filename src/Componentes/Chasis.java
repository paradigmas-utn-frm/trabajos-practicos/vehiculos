package Componentes;

import Vehículos.Producto;

public class Chasis {
    public final String numero; // se califica como final porque el valor sólo será modificado en el constructor
    private Producto pertenece;

    public Chasis(String numero) {
        this.numero = numero;
    }

    public void setPertenece(Producto p){
        pertenece = p;
    }
    
    public Producto getPertenece() {
        return pertenece;
    }
}
