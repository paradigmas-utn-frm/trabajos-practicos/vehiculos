package Componentes;

import Vehículos.Producto;

public class Motor {
    public final String numero; // se califica como final porque el valor sólo será modificado en el constructor
    private Producto instalado;

    public Motor(String n, Producto p) {
        this.numero = n;
        this.instalado = p;
    }

    public Producto getInstalado() {
        return instalado;
    }    
}
