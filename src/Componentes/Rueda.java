package Componentes;

public class Rueda {
    public final String marca; // se califica como final porque el valor sólo será modificado en el constructor

    public Rueda(String marca) {
        this.marca = marca;
    }
}
