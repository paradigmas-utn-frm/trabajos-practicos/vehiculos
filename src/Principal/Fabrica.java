package Principal;

import Vehículos.*;
import Componentes.Chasis;

public class Fabrica {
    public final String nombre;

    public Fabrica(String nombre) {
        this.nombre = nombre; // el uso de this en este caso es obligatorio porque la variable tiene el mismo nombre que el atributo
    }
    // el primer metodo crea autos y el segundo camiones
    public Vehiculo crear(Chasis c, Fabrica f){
        Vehiculo v = new Auto(c, f, c.numero); //el modelo indica que hay una relación de instanciación
        return v;                              //por lo cual la fabrica se encarga de crear productos
                                               //linea 14 y 15 se pueden simplificar en una sola linea: return new Auto(c, f, c.numero); como por ej en linea 20
    }
    
    public Vehiculo crear(Fabrica f,Chasis c){
        return new Camion(c, f, c.numero);
    }   
}
