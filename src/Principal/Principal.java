package Principal;

import Componentes.*;
import Vehículos.*;

public class Principal {
    
    public static void main(String[] args) {
        Fabrica f1 = new Fabrica("Ford");
        Fabrica f2 = new Fabrica("Fiat");
        Chasis c1 = new Chasis("chasis01");
        Vehiculo v = f1.crear(c1, f2); // crea un auto
        c1.setPertenece((Producto)v);
        v.ponerMotor("motor01");
        v.ponerRueda(new Rueda("Testing"));
        //v.ponerRuedas("pirelli", 4);
        ((Producto)v).setPrecio(236.59f);
        ((Producto)v).verDatos();
        
        Chasis c2 = new Chasis("chasis02");
        v = f1.crear(f1, c2); // crea un camion
        c2.setPertenece((Producto)v);
        v.ponerMotor("motor02");
        v.ponerRuedas("goodyear", 8);
        ((Camion)v).setCapCarga(14);
        ((Producto)v).setPrecio(359.85f);
        ((Producto)v).verDatos();
    }
}
