package Vehículos;

import Componentes.Chasis;
import Componentes.Motor;
import Principal.Fabrica;

public class Camion extends Producto{
    private int capCarga;

    public Camion(Chasis c, Fabrica construidoPor, String nro) {
        super(c, construidoPor, nro);
    }

    public int getCapCarga() {
        return capCarga;
    }

    public void setCapCarga(int capCarga) {
        this.capCarga = capCarga;
    }

    public void verDatos(){
        super.verDatos(); // llamo a verDatos del padre y luego agrego cap de carga
        System.out.println("\nCap. carga: "+capCarga);        
    }
    
}
