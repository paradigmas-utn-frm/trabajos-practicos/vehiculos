package Vehículos;
import Principal.Fabrica;
import Componentes.Chasis;
import Componentes.Motor;
import Componentes.Rueda;

public abstract class Producto implements Vehiculo {
    public final Fabrica construidoPor;
    protected Chasis compuesto; //es protegido porque así lo indica el modelo #
    protected Motor impulsado;  //es protegido porque así lo indica el modelo #
    protected Rueda [] usa;     //es protegido porque así lo indica el modelo #
    protected float precio;     //es protegido porque así lo indica el icono (llave amarilla) del modelo 

    public Producto(Chasis c, Fabrica f, String nro) {
        construidoPor = f; // en este caso no es necesario el uso de this, usarlo no está mal pero sería redundante
        compuesto = c;
    }
    public void verDatos(){ // el cuerpo del metodo podria tener otra forma siempre y cuando muestre los datos del producto
        System.out.println("Fabricado por: "+construidoPor.nombre);
        System.out.println("Chasis. "+compuesto.numero);
        System.out.println("Motor: "+impulsado.numero);
        System.out.println("Precio: "+precio);
        for (Rueda r : usa){
            if(r != null) System.out.println("\nRueda: "+r.marca);
        }
            
    }
    public Motor getMotor() {
       return impulsado;
    }
    public Chasis getChasis() {
        return compuesto;
    }
    public Rueda [] getRuedas() {
       return usa;
    }
    public void ponerMotor(String n) {
        this.impulsado = new Motor(n, this);
    }
    public void ponerRuedas(String m, int c) { // este metodo fue agregado por mi se le debe indicar la cantidad de ruedas que tendra el producto
        usa = new Rueda [c];                   // lo he agregado a modo de proponer otra manera de agregar ruedas al producto
        for (int i = 0; i < c; i++)
            usa[i] = new Rueda(m);
    }    
    
     public void ponerRueda(Rueda r) { // suponemos que todos los productos van a tener 4 ruedas, pero podria indicarse la cantidad de las mismas en el constructor
         if(usa == null){
             usa = new Rueda[4];
         }
         for (int i = 0; i < usa.length; i++) {
             if(usa[i] == null){
                usa[i] = r;
                break;
             }
         }
    }    
    public void setPrecio(float p){
        precio = p;
    }
}   