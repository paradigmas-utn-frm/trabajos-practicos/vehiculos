package Vehículos;
import Componentes.*;

public interface Vehiculo {
    Motor getMotor();
    Chasis getChasis();
    Rueda [] getRuedas(); // se puede usar ArrayList
    void ponerMotor(String m);
    void ponerRuedas(String m, int c); //Este método no es parte del modelo
    void ponerRueda(Rueda r);
}
