package Vehículos;

import Componentes.Chasis;
import Componentes.Motor;
import Principal.Fabrica;

public class Auto extends Producto{
    public Auto(Chasis c, Fabrica construidoPor, String nro) {
        super(c, construidoPor, nro);
    }
}
